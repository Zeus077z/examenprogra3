﻿using ExamenDanielRojas.DAL;
using ExamenDanielRojas.WSBCCR;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Xml.Linq;
using WebService;

namespace ExamenDanielRojas
{
    public partial class FrmCambios : Form
    {
        private List<XElement> li;
        private List<string> listaValores;
        private List<string> listaDias;
        private List<Series> datosSeries;
        private CambioDAL ddal;

        public FrmCambios()
        {
            InitializeComponent();
            li = new List<XElement>();
            listaValores = new List<string>();
            listaDias = new List<string>();
            datosSeries = new List<Series>();
            ddal = new CambioDAL();
        }

        private void FrmCambios_Load(object sender, EventArgs e)
        {
            TipoCambio();
        }
        /// <summary>
        /// Función para cargar tipo de cambio, mediante el servidor del BCCR.
        /// Carga mediante bluces for los valores necesarios para usarlos en el chart lineal; Una vez cargados pasan a la base de datos
        /// y después se extraen para su uso
        /// </summary>
        private void TipoCambio()
        {
            try
            {
                string diaInicial = DateTime.Today.AddDays(-30).ToString("dd/MM/yyyy");
                string diaFinal = DateTime.Now.ToString("dd/MM/yyyy");
                wsindicadoreseconomicosSoapClient ws = new wsindicadoreseconomicosSoapClient("wsindicadoreseconomicosSoap");
                string xml = ws.ObtenerIndicadoresEconomicosXML("318", diaInicial, diaFinal, "Daniel", "N", "jrojasm@est.utn.ac.cr", "1E08NTU9CC");
                var indicador = xml.ParseXML<BCCREntities.Datos_de_INGC011_CAT_INDICADORECONOMIC>();
                XDocument xml1 = XDocument.Parse(xml);
                li = xml1.Descendants("NUM_VALOR").ToList();
                foreach (var item in li)
                {
                    string dato = item.Value;
                    listaValores.Add(dato);
                }

                li = xml1.Descendants("DES_FECHA").ToList();
                foreach (var item in li)
                {
                    string dato = item.Value;
                    listaDias.Add(dato);
                }
                double valor = 0;
                for (int i = 0; i < listaValores.Count; i++)
                {
                    string fecha = listaDias[i].Substring(0, 10);
                    valor = Double.Parse(listaValores[i]);
                    datosSeries.Add(new Series { dia = listaDias[i], Serie1 = valor });
                    Series s = new Series()
                    {
                        dia = fecha,
                        Serie1 = valor
                    };
                    ddal.Insertar(s);
                }
                List<Series> cambios = new List<Series>();
                cambios = ddal.CargarCambios();
                chart1.Series.Clear();
                //List<DateTime> fechas = new List<DateTime>();
                //fechas = ddal.CargarFechas();
                chart1.DataBindTable(cambios, "dia");
                chart1.Series[1].ChartType = SeriesChartType.Line;
            }
            catch (Exception)
            {
                MessageBox.Show("Problemas de conexión con el servidor");
            }
            
        }
        /// <summary>
        /// Una vez que el usuario desea salir, se borran los datos de la tabla de la base de datos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmCambios_FormClosing(object sender, FormClosingEventArgs e)
        {
            ddal.EliminarDatos();
        }
    }
}
