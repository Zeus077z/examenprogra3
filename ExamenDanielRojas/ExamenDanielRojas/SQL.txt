
CREATE TABLE tipo_cambio
(
	id serial primary key,
	cambio double precision,
	fecha text
);